# Tiny REST API server with personal wallets and transaction ledger

## Setup

1\. Make sure docker daemon is running:
    
```
   ➜  ~ docker -v
   Docker version 19.03.4, build 9013bf5
```

2\. (Optional) Change the values in .env config file however you need

3\. Execute the following command and wait until it completes:
```
    docker-compose up
```

4\. Enjoy (see [Usage](#Usage))

## Usage

### Postman

Import postman [collection](./postman/Finance.postman_collection.json) 
and [environment](./postman/local_deployment.postman_environment.json) to try out the API endpoints

### Manual

- Get user data:

```
    GET /users/{id}
```
  
  Response example:
```
    { "id": 1, "first_name": "Rich", "last_name": "Boi", "balance": "$0.00" }
```
  
Errors:
- 404 - user not found
  
___
  
- Create a user:

``` 
    POST /users/{id}
```

Request body:
```
    {
        "first_name": "Rich",
        "last_name": "Boi"
    }
```

Response example:
```
    {"id": 1}
```
  
___
  
- Refill a user's wallet:

``` 
    POST /refill/{user_id}
```

Request body:
```
    {
        "amount": 10.0
    }
```

Response example:
```
    {"id": 1}
```

Errors:
- 404 - user not found; 400 - incorrect refill amount

___
  
- Transfer funds from user to user
  
```
    POST /transfer/{sender_id}/{receiver_id}
```

Request body:
```
    {
        "amount": 10.0
    }
```

Response example:
``` 
    { "id": 1 }
```

Errors:
- 404 - sender or receiver not found; 400 - incorrect refill amount; 400 - insufficient sender balance


## Technologies used

- aiohttp: powerful async HTTP server fitting for fast-performing APIs
- PostgreSQL: better concurrency and DB-side tuning capabilities than other solutions
- SQLAlchemy: powerful and developer-friendly ORM and DB management framework 

## Possible improvements

1. Use aiopg instead of psycopg2 to make advantage of async db operations
2. Add multithreading and load-balancing support using Gunicorn/nginx
3. For more complex operations support move business logic from model classes to a separate layer; 
decouple HTTP routes and actual business logic
4. Add logging support
5. Add better db access control