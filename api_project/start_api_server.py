import argparse

from api.main import APIServer


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default=APIServer.DEFAULTS["host"], help="API server host")
    parser.add_argument("--port", type=str, default=APIServer.DEFAULTS["port"], help="API server port")
    parser.add_argument("--env_path", type=str, default=APIServer.DEFAULTS["env_path"],
                        help="Path to environment configuration file with Postgres DB setup data")
    return parser.parse_args()


def run():
    args = parse_args()
    api_server = APIServer(host=args.host, port=args.port, env_path=args.env_path)
    api_server.run()


if __name__ == "__main__":
    run()
