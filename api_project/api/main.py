import json
import logging
import math
import time
from http.client import BAD_REQUEST, NOT_FOUND
from json import JSONDecodeError
from typing import Union

from aiohttp import web
from dotenv import load_dotenv
from sqlalchemy.exc import OperationalError

from db import DBConfig, create_db_engine, Session, init_db, with_session
from db.models import User, Transaction
from util.exceptions import ArgumentError, DBConfigException
from util.helpers import parse_user_id, parse_transaction_amount

routes = web.RouteTableDef()


def json_response(body: Union[dict, list]):
    return web.Response(text=json.dumps(body, default=str),
                        content_type="application/json")


@routes.get('/users/{id:\d+}')
@with_session
async def get_user(request: web.Request, session: Session) -> web.Response:
    """
        Get user data by db id
    :param request: aiohttp web.Request
    :param session: sqlalchemy Session instance
    :return: response with user data json in body
    """
    try:
        user_id = parse_user_id(request.match_info['id'])
    except ArgumentError as e:
        return web.Response(status=BAD_REQUEST, text=str(e))

    user = session.query(User).filter_by(id=user_id).first()
    if not user:
        return web.Response(status=NOT_FOUND)

    # User wallet data is updated here to save processing time during transactions.
    await user.wallet.update_balance(session)
    return json_response(body=user.to_dict())


@routes.post('/users')
@with_session
async def create_user(request: web.Request, session: Session) -> web.Response:
    """

        :param request: aiohttp web.Request
        :param session: sqlalchemy Session instance
        :return: response with created user id in body
    """
    if request.content_type != "application/json":
        return web.Response(status=BAD_REQUEST,
                            text="Only application/json content type is supported")

    try:
        user_data = json.loads(await request.content.read())
    except JSONDecodeError:
        return web.Response(status=BAD_REQUEST,
                            text="Request body must be a valid JSON")

    try:
        user = User.from_dict(user_data)
    except ValueError as e:
        return web.Response(status=BAD_REQUEST,
                            text=str(e))

    user_id = await user.create(session)

    response_text = {
        "id": user_id
    }

    return json_response(body=response_text)


@routes.post('/refill/{receiver_id:\d+}')
@with_session
async def refill(request: web.Request, session: Session) -> web.Response:
    try:
        request_data = json.loads(await request.content.read())
    except JSONDecodeError:
        return web.Response(status=BAD_REQUEST,
                            text="Request body must be a valid JSON")

    try:
        receiver_id = parse_user_id(request.match_info['receiver_id'])
        amount = parse_transaction_amount(request_data.get("amount"))
    except ArgumentError as e:
        return web.Response(status=BAD_REQUEST, text=str(e))

    if not session.query(User).filter_by(id=receiver_id).first():
        return web.Response(status=BAD_REQUEST, text="Receiver not found")

    try:
        transaction = Transaction.from_dict({"receiver_id": receiver_id, "amount": amount})
    except ValueError as e:
        return web.Response(status=BAD_REQUEST,
                            text=str(e))

    transaction_id = await transaction.create(session)

    response_text = {
        "id": transaction_id
    }

    return json_response(response_text)


@routes.post('/transfer/{from_user:\d+}/{to_user:\d+}')
@with_session
async def transfer(request: web.Request, session: Session) -> web.Response:
    try:
        request_data = json.loads(await request.content.read())
    except JSONDecodeError:
        return web.Response(status=BAD_REQUEST,
                            text="Request body must be a valid JSON")

    try:
        sender_id = parse_user_id(request.match_info['from_user'])
        receiver_id = parse_user_id(request.match_info['to_user'])
        amount = parse_transaction_amount(request_data.get("amount"))
    except ArgumentError as e:
        return web.Response(status=BAD_REQUEST, text=str(e))

    sender = session.query(User).filter_by(id=sender_id).first()
    if not sender:
        return web.Response(status=NOT_FOUND, text="Sender not found")
    if not session.query(User).filter_by(id=receiver_id).first():
        return web.Response(status=NOT_FOUND, text="Receiver not found")

    await sender.wallet.update_balance(session)
    if sender.wallet.balance < amount:
        return web.Response(status=BAD_REQUEST, text="Insufficient sender wallet balance")

    try:
        transaction = Transaction.from_dict({"receiver_id": receiver_id, "sender_id": sender_id, "amount": amount})
    except ValueError as e:
        return web.Response(status=BAD_REQUEST,
                            text=str(e))

    transaction_id = await transaction.create(session)

    response_text = {
        "id": transaction_id
    }

    return json_response(body=response_text)


class APIServer:
    MAX_CONNECTION_ATTEMPTS = 10

    DEFAULTS = {
        "host": "0.0.0.0",
        "port": 9988,
        "env_path": None
    }

    def __init__(self, host: str, port: int, env_path: str = None):
        self.log = logging.getLogger("api_server")
        self.log.setLevel("INFO")
        self.log.addHandler(logging.StreamHandler())

        self.host = host
        self.port = port

        # TODO: limit paths and handle exceptions
        if env_path:
            # Load environment vars from .env config file
            load_dotenv(dotenv_path=env_path)

        self.db_engine = None

    @staticmethod
    def db_connection_backoff(attempt: int = 1) -> int:
        return int(math.pow(2, attempt))

    def connect_to_db(self) -> None:
        """
            Connect to db and initialize tables.
            If db is not available outright,
            backoff and try connecting MAX_CONNECTION_ATTEMPTS-1 more times
        :return:
        """
        connection_attempt = 1
        while True:
            if connection_attempt > self.MAX_CONNECTION_ATTEMPTS:
                raise DBConfigException(f"Failed to connect to DB after {connection_attempt} attempts")

            try:
                with self.db_engine.connect():
                    self.log.info("Connected to db")
                    break
            except OperationalError:
                self.log.warning("Failed to connect to db. Retrying")
                time.sleep(self.db_connection_backoff(attempt=connection_attempt))
                connection_attempt += 1

    def run(self) -> None:
        db_config = DBConfig.from_env_vars()

        self.db_engine = create_db_engine(config=db_config)
        Session.configure(bind=self.db_engine)

        self.connect_to_db()
        init_db(db_engine=self.db_engine)
        self.log.info("Verified/created the db")

        app = web.Application()
        # Not needed now, good for debugging: app['db_engine'] = self.db_engine
        app.add_routes(routes)

        web.run_app(app, host=self.host, port=self.port)
