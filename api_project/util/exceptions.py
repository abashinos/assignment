class DBConfigException(Exception):
    pass


class ArgumentError(Exception):
    pass
