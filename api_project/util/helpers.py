from decimal import Decimal
from math import floor

from util.exceptions import ArgumentError


def parse_user_id(user_id: str) -> int:
    try:
        user_id = int(user_id)
        if user_id <= 0:
            raise ValueError()
        return user_id
    except (ValueError, TypeError):
        raise ArgumentError("User id must be a positive integer")


def parse_transaction_amount(amount: str) -> Decimal:
    try:
        amount = float(amount)
        if amount < 0.0:
            raise ValueError()
        # Get rid of not meaningful trailing digits
        amount = floor(amount * 100) / 100.0
        return Decimal(amount)
    except (ValueError, TypeError):
        raise ArgumentError("Transaction amount must be a valid positive numeric value")
