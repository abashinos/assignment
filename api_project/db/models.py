import datetime
from decimal import Decimal

from sqlalchemy import Column, Integer, String, ForeignKey, Numeric, TIMESTAMP, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, Session
from sqlalchemy.sql import label

Base = declarative_base()


class ModelMixin:
    """
        Class for commonly used convenience functions for models
    """

    # Fields required to create a class instance.
    REQUIRED_FIELDS = []

    @classmethod
    def validate_required_fields(cls, data_dict: dict) -> None:
        """
            Validate that all fields listed in cls.REQUIRED_FIELDS are present in payload
        :param data_dict: payload to create a model instance from
        :raises ValueError: error message contains mandatory fields missing in data dict
        :return:
        """
        missing_fields = [
            required_field for required_field
            in cls.REQUIRED_FIELDS
            if not data_dict.get(required_field)
        ]

        if missing_fields:
            raise ValueError(f"Following fields must be defined and non-empty: {', '.join(missing_fields)}")

    @staticmethod
    def safe_commit(session) -> None:
        """
            Wrapper for guaranteed execution of commit/rollback upon query resolution
        :param session: sqlalchemy Session instance
        :return:
        """
        try:
            session.commit()
        except:
            session.rollback()
            raise


class User(Base, ModelMixin):
    REQUIRED_FIELDS = ["first_name", "last_name"]

    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)

    wallet = relationship("Wallet", uselist=False, back_populates="owner", lazy="joined")

    async def create(self, session: Session) -> int:
        """
            Create a user instance along with an empty wallet instance
        :param session: sqlalchemy Session instance
        :return: db id of created object
        """
        session.add(self)
        session.add(Wallet(owner=self))
        super().safe_commit(session)
        return self.id

    @classmethod
    def from_dict(cls, user_data: dict) -> 'User':
        """
            Prepare a new user object from a dict object
        :param user_data
        :return: an instance of User class
        """
        cls.validate_required_fields(data_dict=user_data)
        return cls(first_name=user_data["first_name"], last_name=user_data["last_name"])

    def to_dict(self) -> dict:
        """
            Represent a user as a dict.
        :return: dict object with public user info
        """
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "balance": f"${self.wallet.balance:.2f}"
        }

    def __repr__(self):
        return f"User: {self.first_name} {self.last_name}"


class Wallet(Base, ModelMixin):
    __tablename__ = "wallets"

    id = Column(Integer, primary_key=True)
    owner_id = Column(Integer, ForeignKey(f"{User.__tablename__}.id"), index=True, nullable=False)
    owner = relationship("User", back_populates="wallet")
    balance = Column(Numeric(scale=2, precision=20), default=0.0, nullable=False)
    balance_updated_at = Column(TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)

    async def update_balance(self, session: Session) -> None:
        """
            Update wallet balance and timestamp based on transactions since the last update point
        :param session: sqlalchemy Session instance
        :return:
        """
        # Set current time to avoid missing any transactions
        now = datetime.datetime.utcnow()

        incoming_amount = session.query(label('sum', func.sum(Transaction.amount))).filter(
            Transaction.receiver_id == self.owner_id,
            Transaction.timestamp >= self.balance_updated_at
        ).first().sum
        outgoing_amount = session.query(label('sum', func.sum(Transaction.amount))).filter(
            Transaction.sender_id == self.owner_id,
            Transaction.timestamp >= self.balance_updated_at
        ).first().sum

        self.balance_updated_at = now
        self.balance += (
            Decimal(incoming_amount if incoming_amount else 0.0)
            - Decimal(outgoing_amount if outgoing_amount else 0.0)
        )
        self.safe_commit(session)


class Transaction(Base, ModelMixin):
    __tablename__ = "transactions"

    REQUIRED_FIELDS = ["receiver_id", "amount"]

    id = Column(Integer, primary_key=True)
    sender_id = Column(Integer, ForeignKey(f"{User.__tablename__}.id"), index=True)
    receiver_id = Column(Integer, ForeignKey(f"{User.__tablename__}.id"), index=True, nullable=False)
    amount = Column(Numeric(scale=2, precision=20), nullable=False)
    timestamp = Column(TIMESTAMP, default=datetime.datetime.utcnow, index=True, nullable=False)

    async def create(self, session: Session) -> int:
        """
            Create a transaction instance.
            Transactions with empty sender_id are used to represent wallet refills
        :param session: sqlalchemy Session instance
        :return: db id of created object
        """
        session.add(self)
        super().safe_commit(session)
        return self.id

    @classmethod
    def from_dict(cls, transaction_data: dict) -> 'Transaction':
        """
            Prepare a new transaction object from a dict object
        :param transaction_data
        :return: an instance of Transaction class
        """
        cls.validate_required_fields(data_dict=transaction_data)
        return cls(
            sender_id=transaction_data.get("sender_id"),
            receiver_id=transaction_data["receiver_id"],
            amount=transaction_data["amount"]
        )
