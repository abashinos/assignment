import os
from dataclasses import dataclass
from typing import Callable

from aiohttp import web
from sqlalchemy import create_engine, engine
from sqlalchemy.orm import sessionmaker
from yaml import safe_load

from db.models import Base
from util.exceptions import DBConfigException


@dataclass
class DBConfig:
    DEFAULT_HOST = "db"
    DEFAULT_PORT = 5432

    user: str
    pwd: str
    db: str
    host: str = DEFAULT_HOST
    port: int = DEFAULT_PORT

    # Unused
    @classmethod
    def from_yaml_file(cls, file_path: str) -> 'DBConfig':
        try:
            with open(file_path) as yaml_file_contents:
                yaml_dict = safe_load(yaml_file_contents)
        except OSError as e:
            raise DBConfigException(f"DB config file has some issues: {e}")

        try:
            return cls(host=yaml_dict['host'],
                       port=yaml_dict.get('port', cls.DEFAULT_PORT),
                       user=yaml_dict['user'],
                       pwd=yaml_dict['pwd'],
                       db=yaml_dict['db'])
        except KeyError as e:
            raise DBConfigException(f"Required config key missing: {e}")

    @classmethod
    def from_env_vars(cls) -> 'DBConfig':
        return cls(
            host=os.environ.get('POSTGRES_HOST', cls.DEFAULT_HOST),
            port=os.environ.get('POSTGRES_PORT', cls.DEFAULT_PORT),
            user=os.environ['POSTGRES_USER'],
            pwd=os.environ['POSTGRES_PASSWORD'],
            db=os.environ['POSTGRES_DB']
        )


Session = sessionmaker()


def with_session(func: Callable):
    """
        Open and pass an sqlalchemy session to the decorated coroutine, safely close after function exits
    :param func: decorated function
    :return:
    """

    async def wrapper(*args, **kwargs):
        session = Session()
        try:
            results = await func(*args, session=session, **kwargs)
        finally:
            session.close()
        return results

    return wrapper


def create_db_engine(config: DBConfig) -> engine.Engine:
    """
        Construct a psql connection url and create sqlalchemy.Engine instance
    :param config:
    :return: sqlalchemy.Engine instance
    """
    return create_engine(f'postgresql://{config.user}:{config.pwd}@{config.host}:{config.port}/{config.db}',
                         pool_size=50,
                         max_overflow=100)


def init_db(db_engine: engine.Engine) -> None:
    """
        Initialize the database models
    :param db_engine: sqlalchemy Engine instance
    :return:
    """
    Base.metadata.create_all(db_engine)
