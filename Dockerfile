FROM python:3.8

RUN groupadd -r api_user && useradd --no-log-init -r -g api_user api_user

COPY api_project /api_project
COPY .env /api_project/.env

RUN chown -R api_user:api_user /api_project && cd /api_project && pip3 install -r requirements.txt

# TODO: make exposed port dynamic via docker-compose
EXPOSE 9988

USER api_user
WORKDIR /api_project

ENTRYPOINT ["python3", "-m", "start_api_server"]
CMD ["--port", "9988", "--env_path", "/api_project/.env"]